Particle Fleet CIL-Stripped Libraries
=====================================

A small collection of libraries taken from Particle Fleet: Emergence (as well as MonoMod), for the purpose of modding the game.

The libraries have been CIL-stripped, which means that **they contain no "actual" C# code** - just the definitions of classes and methods, so they can be referenced in mods and shared without worries.

The libraries have been stripped with mono-cil-strip and not all libraries have been included for the sake of size and simplicity - if you find the need for other libraries, please tell me or make an issue/pull request.